package com.example.davaleba5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*

class Profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init ()
    }
    private fun init () {
        Glide.with(this)
            .load("https://coolwallpapers.me/picsup/2946086-nature-photography___landscape-nature-wallpapers.jpg")
            .into(coverPictureImageView);

        Glide.with(this)
            .load("https://pickaface.net/gallery/avatar/Victory1983538741e2d24fa.png")
            .into(profilePictureImageView);

    }
}