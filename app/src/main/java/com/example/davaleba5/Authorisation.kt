package com.example.davaleba5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import kotlinx.android.synthetic.main.authorisation.*

class Authorisation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.authorisation)
        init()
    }

    private fun init() {
        AuthorisationButton.setOnClickListener {
            if (LoginEditText.text.toString().isNotEmpty() && PasswordEditText.text.toString()
                    .isNotEmpty()
            ) {
                openProfile()
            }
        }
    }

    private fun openProfile() {
        val intent = Intent(this, Profile::class.java)
        startActivity(intent)
    }
}





